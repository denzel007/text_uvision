<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Material Design for Bootstrap fonts and icons -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

        <!-- Material Design for Bootstrap CSS -->
        <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
        <link href="https://fezvrasta.github.io/bootstrap-material-design/assets/css/docs.min.css" rel="stylesheet">
        <style>
            .bd-navbar {
                position: sticky; 
                top: 0;
                z-index: 1071;
            }
        </style>
        <title>Tests Uvision</title>
    </head>
    <body>
        <header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar">


            <div class="container d-flex justify-content-between">
                <a href="#" class="navbar-brand d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>
                    <strong>Sticky Header</strong>
                </a>

            </div>

        </header>

        <main role="main">

            <section class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading">Test Uvision - Question 4</h1>
                    <p class="text-justify">[ [ A : 1 ],[ B : 2 ] ,[ P : 16 ] , [ B : 2 ] , [ C : 3 ] , [ A : 1 ], [ O : 15 ] , [ C : 3 ] ……. Infinite ] - using
                        only php, please create one array that will have all A-Z letters sorted in their right order using
                        lowest time complexity.</br>
                        ● All english letters are lowercase</br>
                        ● All letters’ value is their real A-Z index
                    </p>
                </div>
            </section>
            <div class="album py-5 bg-light">
                <div class="container" >

                    $inputArray = [ ['A' => 1], [ 'B' => 2 ] ,[ 'P' => 16 ] , [ 'B' => 2 ] , [ 'C' => 3 ] , [ 'A' => 1 ], [ 'O' => 15 ] , [ 'C' => 3 ], [ 'R' => 18 ], [ 'N' => 14 ], [ 'U' => 21 ], [ 'B' => 2 ]];
                    </br></br>Result Array:
                    <? 
                    require_once('q4.php');
                    ?>

                </div>
            </div>     

        </main>

        <footer class="text-muted">
            <div class="container">
                <p class="float-right">
                    <a href="#">Back to top</a>
                </p>
                <p>Test Uvision Example</p>
            </div>
        </footer>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function () {
                $('body').bootstrapMaterialDesign();
            });
        </script>
    </body>
</html>