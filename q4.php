<?

$inputArrayExtended = [ 
    ["A" => 1], 
    [ "B" => 2 ],
    [ "P" => 16 ], 
    [ "B" => 2 ], 
    [ "C" => 3 ], 
    [ "A" => 1 ], 
    [ "O" => 15 ], 
    [ "C" => 3 ], 
    [ "R" => 18 ], 
    [ "N" => 14 ], 
    [ "U" => 21 ], 
    [ "B" => 2 ]
    ];

$inputArray = $inputArrayExtended;

$indexesArr = [];
$tempArr = [];
$resultArr = [];

foreach ($inputArray as $value) {
    $toLowerCase = strtolower(key($value));
    $indexesArr[$toLowerCase] = current($value);
    $tempArr[] = $toLowerCase;
}

asort($tempArr);

foreach ($tempArr as $value) {
    $resultArr[] = [$value => $indexesArr[$value]];
}

echo '<pre>';
print_r($resultArr);
echo '</pre>';
